interface CommandLineArguments {
    ip: string,
    token: string,
    location: Location,
}

interface Location {
    longitude: number,
    latitude: number,
}
